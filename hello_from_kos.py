def hello_from_kos(name: str) -> str:
    return f"{name}! Hello from Kostyantin!"


if __name__ == "__main__":
    name = "Stepan"
    print(hello_from_kos(name))
